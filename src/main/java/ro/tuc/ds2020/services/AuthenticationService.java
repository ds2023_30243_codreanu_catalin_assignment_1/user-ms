package ro.tuc.ds2020.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.util.Objects;
import java.util.Optional;

@Service
public class AuthenticationService {

    private final PersonRepository personRepository;

    public AuthenticationService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    // Method to authenticate the user based on the provided credentials
    public boolean authenticateUser(String username, String password) {
        Optional<Person> user = personRepository.findByUsername(username);

        if(!user.isPresent()){
            return false;
        }

        Person foundUser = user.get();

        if(!foundUser.getPassword().equals(password)){
            return false;
        }

        return true;
    }

    public String hashPassword(String plainPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(plainPassword);
    }
}
