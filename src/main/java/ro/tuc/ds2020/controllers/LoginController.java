package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.config.JwtUtil;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.models.LoginResponseModel;
import ro.tuc.ds2020.security.CustomAuthenticationProvider;
import ro.tuc.ds2020.services.PersonService;

import javax.servlet.http.HttpSession;
import java.util.Map;
@RestController
@RequestMapping(value = "/login")
public class LoginController {

    private final CustomAuthenticationProvider customAuthenticationProvider;
    private final PersonService personService;
    ObjectMapper objectMapper;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    public LoginController(CustomAuthenticationProvider customAuthenticationProvider, PersonService personService) {
        this.customAuthenticationProvider = customAuthenticationProvider;
        this.personService = personService;
        this.objectMapper = new ObjectMapper();
    }

    @PostMapping
    public ResponseEntity<String> login(@RequestBody Map<String, String> loginRequest) throws JsonProcessingException {
        String username = loginRequest.get("username");
        String password = loginRequest.get("password");

        LoginResponseModel loginResponse = new LoginResponseModel();
        Authentication authentication = new UsernamePasswordAuthenticationToken(username, password);

        try {
            customAuthenticationProvider.authenticate(authentication);

            Person user = personService.getUserByUsername(username).get();
            final String jwt = jwtTokenUtil.generateToken(user);

            loginResponse.setUserId(user.getId());
            loginResponse.setRole(user.getRole().toString());
            loginResponse.setToken(jwt);

            String json = objectMapper.writeValueAsString(loginResponse);

            return new ResponseEntity<>(json, HttpStatus.OK);

        } catch (BadCredentialsException e) {
            loginResponse.setMessage("Wrong credentials!");
            String json = objectMapper.writeValueAsString(loginResponse);

            return new ResponseEntity<>(json, HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping
    public ResponseEntity<String> logout(HttpSession session) {
        LoginResponseModel logoutResponse = new LoginResponseModel();

        session.invalidate();

        logoutResponse.setMessage("Logged out successfully");

        try {
            String json = objectMapper.writeValueAsString(logoutResponse);
            return new ResponseEntity<>(json, HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Error processing JSON", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

