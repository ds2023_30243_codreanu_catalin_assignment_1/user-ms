package ro.tuc.ds2020.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class LoginResponseModel {
    @JsonProperty("token")
    private String token;

    @JsonProperty("role")
    private String role;

    @JsonProperty("message")
    private String message;

    @JsonProperty("userId")
    private UUID userId;

    @JsonProperty("username")
    private String username;

    public LoginResponseModel() {
    }

    public void setToken(String id) {
        this.token = id;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setMessage(String message) {

        this.message = message;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}