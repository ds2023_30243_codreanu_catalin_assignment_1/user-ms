package ro.tuc.ds2020.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SessionValidationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false); // Retrieve the session, do not create a new one if it doesn't exist

        if (session != null && session.getAttribute("userId") != null) {
            // Perform any specific validation checks based on your application's requirements
            // You can check for specific attributes in the session or perform other checks here
            String userId = (String) session.getAttribute("userId");
            if (yourValidationLogic(userId)) {
                // Session is valid, continue with the desired operation
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println("Session is valid.");
            } else {
                // Session is invalid, handle the error accordingly
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getWriter().println("Session is invalid.");
            }
        } else {
            // No session or session attribute found, handle the error accordingly
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.getWriter().println("No session found.");
        }
    }

    private boolean yourValidationLogic(String userId) {
        // Your custom validation logic goes here
        // You might perform checks based on the userId or any other session attributes
        // Return true if the session is valid; otherwise, return false
        return true;
    }
}
