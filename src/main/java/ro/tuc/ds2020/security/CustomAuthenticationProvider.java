package ro.tuc.ds2020.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.AuthenticationService;
import ro.tuc.ds2020.services.PersonService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private final AuthenticationService authenticationService;
    private final PersonService personService;

    @Autowired
    public CustomAuthenticationProvider(AuthenticationService authenticationService, PersonService personService) {
        this.authenticationService = authenticationService;
        this.personService = personService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        boolean isAuthenticated = authenticationService.authenticateUser(username, password);
        Optional<Person> user = personService.getUserByUsername(username);

        if (isAuthenticated && user.isPresent()) {
            List<GrantedAuthority> authorities = new ArrayList<>();

            return new UsernamePasswordAuthenticationToken(username, password, authorities);
        } else {
            throw new BadCredentialsException("Invalid credentials");
        }
    }


    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}