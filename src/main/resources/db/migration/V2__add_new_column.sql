-- Example SQL migration script to add new columns to the Person table

-- Add the username and password columns to the Person table
--ALTER TABLE Person
    --ADD COLUMN username VARCHAR(255),
--ADD COLUMN password VARCHAR(255);

-- Update any existing NULL values to a default value or appropriate value
--UPDATE Person
--SET username = 'username'
--WHERE username IS NULL;

--UPDATE Person
--SET password = 'password'
--WHERE password IS NULL;

-- Modify the column to not allow NULL values
--ALTER TABLE Person
    --ALTER COLUMN username SET NOT NULL;
--ALTER COLUMN password SET NOT NULL;
