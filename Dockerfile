# Use an official OpenJDK runtime as a parent image
FROM openjdk:17

# Set the working directory
WORKDIR /usr/src/app

# Copy the JAR file into the container at /usr/src/app
COPY target/ds-2020-0.0.1-SNAPSHOT.jar .

# Expose the port on which the app will run
EXPOSE 8080

# Specify the command to run on container start
CMD ["java", "-jar", "ds-2020-0.0.1-SNAPSHOT.jar"]
